package com.desafiotecnico.desafiotecnico.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Constantes {
    public static final String PATH_ARQUIVOS_ENTRADA = System.getProperty("user.home")+"/data/in";
    public static final String PATH_ARQUIVOS_SAIDA = System.getProperty("user.home")+"/data/out";
}
