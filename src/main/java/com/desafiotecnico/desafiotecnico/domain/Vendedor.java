package com.desafiotecnico.desafiotecnico.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Vendedor implements DadosImportados{
    private Integer id;
    private String cpf;
    private String nome;
    private Double salario;
}
