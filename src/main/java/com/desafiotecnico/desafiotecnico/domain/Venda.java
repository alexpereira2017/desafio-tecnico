package com.desafiotecnico.desafiotecnico.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Venda implements DadosImportados, Comparable<Venda> {
    private Integer id;
    private String vendedor;
    private List<Item> itens;
    private Double valorTotal = 0D;

    @Override
    public int compareTo(Venda o) {
        return new Double(valorTotal).compareTo( o.valorTotal);
    }
}
