package com.desafiotecnico.desafiotecnico.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Item {
    private Integer id;
    private Integer quantidade;
    private Double preco;
}
