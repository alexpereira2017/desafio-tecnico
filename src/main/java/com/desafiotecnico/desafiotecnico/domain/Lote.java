package com.desafiotecnico.desafiotecnico.domain;

import com.desafiotecnico.desafiotecnico.processor.PadraoProcessor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class Lote {
    private String nomeArquivo;
    private List<DadosImportados> dados = new ArrayList<>();

    public void adicionarItem(DadosImportados v) {
        dados.add(v);
    }
}
