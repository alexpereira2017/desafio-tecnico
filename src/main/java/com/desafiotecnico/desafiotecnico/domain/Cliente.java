package com.desafiotecnico.desafiotecnico.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cliente implements DadosImportados {
    private Integer id;
    private String cnpj;
    private String nome;
    private String area;
}
