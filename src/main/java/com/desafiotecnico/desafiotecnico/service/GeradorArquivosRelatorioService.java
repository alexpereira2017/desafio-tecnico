package com.desafiotecnico.desafiotecnico.service;

import com.desafiotecnico.desafiotecnico.domain.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class GeradorArquivosRelatorioService {

    public void criar(List<Lote> dadosCarregados) {

        AtomicLong second = new AtomicLong(Instant.now().getEpochSecond());

        dadosCarregados. forEach(lote -> {
            List<Venda> vendas = retornarSomenteVendas(lote);
            String nomeArquivoProcessado = Constantes.PATH_ARQUIVOS_SAIDA + "/" + second.getAndIncrement() + ".done.dat";
            String conteudo = "Relatório de Processamento: \n";
            conteudo += "Quantidade de clientes no arquivo de entrada: "+Integer.valueOf(calcularQuantidadeClientes(lote))+"\n";
            conteudo += "Quantidade de vendedor no arquivo de entrada: "+Integer.valueOf(calcularQuantidadeVendedores(lote))+"\n";
            conteudo += "ID da venda mais cara: "+maiorVenda(vendas).getId()+"\n";
            conteudo += "O pior vendedor: "+classificarPiorVendedor(vendas);

            try {
                Files.write(Paths.get(nomeArquivoProcessado), conteudo.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private int calcularQuantidadeClientes(Lote lote) {
        return (int) lote
                .getDados()
                .stream()
                .filter(Cliente.class::isInstance)
                .count();
    }

    private int calcularQuantidadeVendedores(Lote lote) {
        return (int) lote
                .getDados()
                .stream()
                .filter(Vendedor.class::isInstance)
                .count();
    }

    private Double calcularMaiorVenda(Lote lote) {
        final Double maior = lote
                .getDados()
                .stream()
                .filter(Venda.class::isInstance)
                .map(v -> ((Venda) v).getValorTotal())
                .max(Comparator.naturalOrder())
                .get();
        return maior;
    }

    private Venda maiorVenda (List<Venda> vendas) {
        Collections.sort(vendas, Collections.reverseOrder());
        return (Venda) vendas.get(0);
    }

    private String classificarPiorVendedor(List<Venda> vendas) {
        Map<String, Double> totais = calcularTotaisPorVendedor(vendas);

        Map<String, Double> totaisOrdenados =
                totais.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue())
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (e1, e2) -> e1, LinkedHashMap::new));

        return (String) totaisOrdenados.keySet().toArray()[0];
    }

    private Map<String, Double> calcularTotaisPorVendedor(List<Venda> vendas) {
        Map<String, Double> totaisPorVendedor = new HashMap<>();
        vendas.forEach(v -> {
            String key = v.getVendedor();
            if (!totaisPorVendedor.containsKey(key)) {
                totaisPorVendedor.put(key, 0D);
            }
            final Double aDouble = totaisPorVendedor.get(key);
            totaisPorVendedor.replace(key, aDouble, aDouble + v.getValorTotal());
        });
        return totaisPorVendedor;
    }

    private List<Venda> retornarSomenteVendas(Lote lote) {
        List<Venda> vendas = new ArrayList<>();
        for (DadosImportados dadosImportados : lote
                .getDados()) {
            if (dadosImportados instanceof Venda) {
                vendas.add((Venda) dadosImportados);
            }
        }
        return vendas;
    }
}
