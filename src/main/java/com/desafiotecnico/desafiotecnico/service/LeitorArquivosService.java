package com.desafiotecnico.desafiotecnico.service;

import com.desafiotecnico.desafiotecnico.domain.Constantes;
import com.desafiotecnico.desafiotecnico.domain.Lote;
import com.desafiotecnico.desafiotecnico.util.ConsumerWrapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class LeitorArquivosService {

    private LeitorLinhasService leitorLinhas;

    private final String[] EXTENSOES_PERMITIDAS = {"dat"};

    @Autowired
    public LeitorArquivosService(LeitorLinhasService leitorLinhas) {
        this.leitorLinhas = leitorLinhas;
    }

    public List<Lote> carregar() {
        File dir = new File(Constantes.PATH_ARQUIVOS_ENTRADA);
        List<File> files = (List<File>) FileUtils.listFiles(dir, EXTENSOES_PERMITIDAS, true);
        List<Lote> lotes = new ArrayList();

        files.forEach(
                ConsumerWrapper.consumerWrapper(
                        arquivo -> lotes.add(this.parseArquivo(arquivo)),
                        Exception.class
                )
        );
        return lotes;
    }

    private Lote parseArquivo(File arquivoLote)  {
        FileInputStream input = null;
        Lote lote = null;
        try {
            input = FileUtils.openInputStream(arquivoLote);
            String content = IOUtils.toString(input, "UTF-8");
            String[] linhas = content.split("\r\n");
            lote = leitorLinhas.parse(linhas);
            lote.setNomeArquivo(arquivoLote.getName());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage()+" Erro ocorrido no arquivo: "+arquivoLote.getName() );
        }
        return lote;
    }


}