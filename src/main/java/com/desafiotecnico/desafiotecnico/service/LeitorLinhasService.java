package com.desafiotecnico.desafiotecnico.service;

import com.desafiotecnico.desafiotecnico.domain.DadosImportados;
import com.desafiotecnico.desafiotecnico.domain.Lote;
import com.desafiotecnico.desafiotecnico.processor.PadraoProcessor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.ValidationException;
import java.util.List;
import java.util.Objects;

@Service
public class LeitorLinhasService {
    private List<PadraoProcessor> processadorList;

    @Autowired
    public LeitorLinhasService(List<PadraoProcessor> processadorList) {
        this.processadorList = processadorList;
    }

    public Lote parse(String[] linhas) throws ValidationException {
        Lote lote = new Lote();
        for (String linha : linhas) {
            String tipo = linha.substring(0,3);
            PadraoProcessor processador = getProcessor(linha, tipo);
            if (Objects.isNull(processador)) {
                throw new IllegalArgumentException("Não foi encontrado um processador para a linha");
            }
            DadosImportados dadosImportados = processador.executar(linha);
            lote.adicionarItem(dadosImportados);
        }
        return lote;
    }

    private PadraoProcessor getProcessor(String linha, String tipo) {
        return processadorList
                .stream()
                .filter(processor -> processor.getCodigoTipo().equals(tipo) )
                .findFirst()
                .orElse(null);
    }
}
