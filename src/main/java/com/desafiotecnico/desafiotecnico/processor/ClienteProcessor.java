package com.desafiotecnico.desafiotecnico.processor;

import com.desafiotecnico.desafiotecnico.domain.Cliente;
import org.springframework.stereotype.Component;

import javax.xml.bind.ValidationException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ClienteProcessor extends PadraoProcessor {

    public static String CODIGO_TIPO_LOTE = "002";

    @Override
    public Cliente executar(String linha) throws ValidationException {
        Cliente cliente = new Cliente();
        String pattern = "(\\d*)ç(\\d*)ç([A-Za-z\\s\\D]*)ç([A-Za-z]*)";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(linha);

        if (!m.find()) {
            throw new ValidationException("Cliente não encontrado na linha de dados informada ("+linha+")");
        }
        cliente.setCnpj(m.group(2));
        cliente.setNome(m.group(3));
        cliente.setArea(m.group(4));
        return cliente;
    }

    @Override
    public String getCodigoTipo() {
        return this.CODIGO_TIPO_LOTE;
    }
}
