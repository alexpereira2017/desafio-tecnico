package com.desafiotecnico.desafiotecnico.processor;

import com.desafiotecnico.desafiotecnico.domain.DadosImportados;
import com.desafiotecnico.desafiotecnico.domain.Vendedor;
import org.springframework.stereotype.Component;

import javax.xml.bind.ValidationException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class VendedorProcessor extends PadraoProcessor {

    public static String CODIGO_TIPO_LOTE = "001";

    @Override
    public DadosImportados executar(String linha) throws ValidationException {

        Vendedor vendedor = new Vendedor();
        String pattern = "(\\d*)ç(\\d*)ç([A-Za-z\\s\\D]*)ç(\\d*)";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(linha);

        if (!m.find()) {
            throw new ValidationException("Vendedor não encontrado na linha de dados informada ("+linha+")");
        }
        vendedor.setCpf(m.group(2));
        vendedor.setNome(m.group(3));
        vendedor.setSalario(Double.parseDouble(m.group(4)));

        return vendedor;
    }

    @Override
    public String getCodigoTipo() {
        return this.CODIGO_TIPO_LOTE;
    }
}
