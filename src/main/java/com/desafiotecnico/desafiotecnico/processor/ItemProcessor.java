package com.desafiotecnico.desafiotecnico.processor;

import com.desafiotecnico.desafiotecnico.domain.DadosImportados;
import com.desafiotecnico.desafiotecnico.domain.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ItemProcessor {

    public List<Item> executar(String itensLinha) {
        List<Item> itens = new ArrayList<>();
        final String[] split = itensLinha.split(",");
        for (String linha : split) {
            final String[] dados = linha.split("-");
            itens.add(new Item(
                    Integer.valueOf(dados[0]),
                    Integer.valueOf(dados[1]),
                    Double.parseDouble(dados[2]))
            );
        }
        return itens;
    }
}
