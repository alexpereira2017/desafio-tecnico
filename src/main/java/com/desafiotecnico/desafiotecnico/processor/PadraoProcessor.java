package com.desafiotecnico.desafiotecnico.processor;

import com.desafiotecnico.desafiotecnico.domain.DadosImportados;

import javax.xml.bind.ValidationException;


public abstract class PadraoProcessor {
    public String CODIGO_TIPO_LOTE;
    public abstract DadosImportados executar(String linha) throws ValidationException;
    public abstract String getCodigoTipo();
}
