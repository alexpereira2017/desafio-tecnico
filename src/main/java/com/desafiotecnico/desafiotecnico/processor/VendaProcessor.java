package com.desafiotecnico.desafiotecnico.processor;

import com.desafiotecnico.desafiotecnico.domain.DadosImportados;
import com.desafiotecnico.desafiotecnico.domain.Item;
import com.desafiotecnico.desafiotecnico.domain.Venda;
import org.springframework.stereotype.Component;

import javax.xml.bind.ValidationException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class VendaProcessor extends PadraoProcessor {

    public static String CODIGO_TIPO_LOTE = "003";

    @Override
    public DadosImportados executar(String linha) throws ValidationException {
        Venda venda = new Venda();
        String pattern = "(\\d*)ç(\\d*)ç(\\[(.*?)\\])ç([A-Za-z\\s\\D]*)";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(linha);

        if (!m.find()) {
            throw new ValidationException("Venda não encontrada na linha de dados informada ("+linha+")");
        }

        ItemProcessor itemProcessor = new ItemProcessor();
        venda.setId(Integer.valueOf(m.group(2)));
        List<Item> itens = itemProcessor.executar(m.group(4));
        venda.setItens(itens);
        venda.setValorTotal(itens.stream().mapToDouble(i -> i.getPreco() * i.getQuantidade()).sum());
        venda.setVendedor(m.group(5));
        return venda;
    }

    @Override
    public String getCodigoTipo() {
        return this.CODIGO_TIPO_LOTE;
    }
}
