package com.desafiotecnico.desafiotecnico.schedule;

import com.desafiotecnico.desafiotecnico.service.LeitorArquivosService;
import com.desafiotecnico.desafiotecnico.domain.Lote;
import com.desafiotecnico.desafiotecnico.service.GeradorArquivosRelatorioService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;
import java.util.List;

@Slf4j
@Configuration
@EnableScheduling
public class ScheduledConfiguration {

    @Autowired
    LeitorArquivosService leitor;

    @Autowired
    GeradorArquivosRelatorioService relatorios;

    @Scheduled(fixedRate = 30000)
    public void executeTask() {
        log.info("Executando o processamento de arquivos em "+ new Date());
        List<Lote> dadosCarregados = leitor.carregar();
        log.info(String.valueOf(dadosCarregados.get(0)));
        relatorios.criar(dadosCarregados);
        log.info("Processamento de arquivos encerrado em "+ new Date());
    }

}
