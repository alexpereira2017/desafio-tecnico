package com.desafiotecnico.desafiotecnico.processor;

import com.desafiotecnico.desafiotecnico.domain.Cliente;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.ValidationException;

import static org.junit.jupiter.api.Assertions.*;


public class ClienteProcessorTest {

    private static PadraoProcessor processador;

    @Before
    public void inicializar() {
        processador = new ClienteProcessor();
    }

    @Test
    public void executarComSucesso() throws ValidationException {

        // Given
        String linha = "002ç2345675434544345çJose Caçapava da SilvaçRural";

        // When
        Cliente cliente = (Cliente) processador.executar(linha);

        // Then
        assertEquals("2345675434544345", cliente.getCnpj());
        assertEquals("Jose Caçapava da Silva", cliente.getNome());
        assertEquals("Rural", cliente.getArea());
    }

    @Test(expected = ValidationException.class)
    public void executarComErro() throws ValidationException {

        // Given
        String linha = "2345675434544345çEdvaldo Pereira Paiva";

        // When
        processador.executar(linha);

        // Then
        // ValidationException
    }
}
