package com.desafiotecnico.desafiotecnico.service;

import com.desafiotecnico.desafiotecnico.processor.ClienteProcessor;
import com.desafiotecnico.desafiotecnico.processor.PadraoProcessor;
import com.desafiotecnico.desafiotecnico.processor.VendaProcessor;
import com.desafiotecnico.desafiotecnico.processor.VendedorProcessor;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.bind.ValidationException;
import java.util.ArrayList;
import java.util.List;

public class LeitorLinhasServiceExcecoesTest {

    @Autowired
    private LeitorLinhasService leitorLinhasService;

    @Before
    public void inicializar() {
        List<PadraoProcessor> processadorList = new ArrayList<>();
        processadorList.add(new ClienteProcessor());
        processadorList.add(new VendaProcessor());
        processadorList.add(new VendedorProcessor());
        leitorLinhasService = new LeitorLinhasService(processadorList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void lancarExcecaoCasoNaoExistaProcessadorDeLinha() throws ValidationException {
        String[] linhas = new String[1];

        // Given
        linhas[0] = "005ç3245678865434çPauloç40000.99";
        try {

            // When
            leitorLinhasService.parse(linhas);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }
}
