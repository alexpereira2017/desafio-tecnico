package com.desafiotecnico.desafiotecnico.service;

import com.desafiotecnico.desafiotecnico.domain.Cliente;
import com.desafiotecnico.desafiotecnico.domain.Lote;
import com.desafiotecnico.desafiotecnico.domain.Venda;
import com.desafiotecnico.desafiotecnico.domain.Vendedor;
import com.desafiotecnico.desafiotecnico.processor.ClienteProcessor;
import com.desafiotecnico.desafiotecnico.processor.PadraoProcessor;
import com.desafiotecnico.desafiotecnico.processor.VendaProcessor;
import com.desafiotecnico.desafiotecnico.processor.VendedorProcessor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.bind.ValidationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)//@SpringBootTest(classes = { LeitorLinhasService.class, PadraoProcessor.class, ClienteProcessor.class})
@EnableConfigurationProperties
public class LeitorLinhasServiceTest {

    private LeitorLinhasService leitorLinhasService;

    private PadraoProcessor padraoProcessor;

    private String linha;
    private Class entidade;

    public LeitorLinhasServiceTest(String linha, Class entidade) {
        this.linha = linha;
        this.entidade = entidade;
    }

    @Before
    public void inicializar() {
        List<PadraoProcessor> processadorList = new ArrayList<>();
        processadorList.add(new ClienteProcessor());
        processadorList.add(new VendaProcessor());
        processadorList.add(new VendedorProcessor());
        leitorLinhasService = new LeitorLinhasService(processadorList);
    }

    @Parameterized.Parameters(name = "Entidade de >> {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"001ç3245678865434çPauloç40000.99", Vendedor.class},
                {"002ç2345675434544345çJose da SilvaçRural", Cliente.class},
                {"003ç011ç[1-10-100,2-30-2.50,3-40-3.10]çRonaldo Rômulo", Venda.class}
        });
    }

    @Test
    public void executarComSucesso() {

        // Given
        String[] linhas = new String[1];
        linhas[0] = this.linha;
        Lote lote = null;
        try {

            //When
            lote = leitorLinhasService.parse(linhas);
        } catch (ValidationException e) {
            e.printStackTrace();
        }

        // Then
        assertThat(lote.getDados().get(0), instanceOf(this.entidade));
    }

}
